﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PositiveOrNegative
{
    class Program
    {
        static void Main(string[] args)
        {
            //Introduces the purpose of the program to the user.
            Console.WriteLine("This program will ask you for two integers" +
                              "\nand help you determine if the multiplied" +
                              "\nresult should be positve or negative.");
            Console.WriteLine();

            //Asks user for the first integer.
            Console.Write("Please input first integer: ");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            //Asks user for the second integer.
            Console.Write("Please input second interger: ");
            int secondNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            //Run if/else statements to figure out result
            if ((firstNumber > 0 && secondNumber > 0) || (firstNumber < 0 && secondNumber < 0))
                Console.WriteLine("The multiplied result will be positive.");                    
            else if ((firstNumber == 0 || secondNumber == 0))
                Console.WriteLine("The result will be zero.");
            else
                Console.WriteLine("The multiplied result will be negative");

            Console.ReadKey();
        }
    }
}
