﻿using System;

namespace CylinderCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            //Greeting to the user
            Console.WriteLine("This program will calculate the volume " +
                              "\n and surface area of a cylinder \n");
            
            //Obtaining height and radius from user
            Console.Write("What is the height of your cylinder? ");
            float h = Convert.ToSingle(Console.ReadLine());
            Console.Write("What is the radius of your cylinder? ");
            float r = Convert.ToSingle(Console.ReadLine());

            //Formulas for volume and surface area
            double v = Math.PI * Math.Pow(r, 2) * h;               //storing volume to variable 
            double surfaceArea = 2 * Math.PI * r * (r + h);        //storing surface area to variable
            
            //Writing formulated answers to the user
            Console.WriteLine("The volume of your cylinder is: " + v);
            Console.WriteLine("The surface area of your cylinder is: " + surfaceArea);
        }
    }
}