﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program will ask you to type in two numbers" +
                              "\nand to assign the type of math operation you would " +
                              "\nlike to perform on the two numbers.");
            Console.WriteLine();

            Console.Write("Type in the first number: ");
            int firstNum = Convert.ToInt32(Console.ReadLine());
            Console.Write("Type in the second number: ");
            int secondNum = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("The next statement will prompt you to input the type" +
                              "\nof operation you would like to perform.\n" +
                              "\nThe following are acceptable characters:" +
                              "\nAddition: '+' " +
                              "\nSubtraction: '-'" +
                              "\nMultiplation: '*'" +
                              "\nDivision: '/'" +
                              "\nRemainder: '%'" +
                              "\nPower: '^'");

            Console.WriteLine();
            Console.Write("Type in a math operation: ");
            string operation = Console.ReadLine();

            switch(operation)
            {
                case "*":
                    Console.WriteLine($"{firstNum} * {secondNum} = " + (firstNum * secondNum));
                    break;
                case "/":
                    Console.WriteLine($"{firstNum} / {secondNum} = " + (firstNum / secondNum));
                    break;
                case "+":
                    Console.WriteLine($"{firstNum} + {secondNum} = " + (firstNum + secondNum));
                    break;
                case "-":
                    Console.WriteLine($"{firstNum} - {secondNum} = " + (firstNum - secondNum));
                    break;
                case "%":
                    Console.WriteLine($"{firstNum} % {secondNum} = " + (firstNum % secondNum));
                    break;
                case "^":
                    Console.WriteLine($"{firstNum} ^ {secondNum} = " + Math.Pow(firstNum, secondNum));
                    break;
                default:
                    Console.WriteLine("That's not a valid operator.");
                    break;
            }
            Console.ReadKey();
        }
    }
}
